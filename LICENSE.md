These exercises were designed by Thore Husfeldt.

![Creative Commons License](http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png)

This code are licensed under a [Creative Commons
Attribution-NonCommercial-NoDerivs 3.0 Unported
License](http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US).
However, permission is granted to modify and re-distribute this code in an academic, educational setting.
In fact, you are encouraged to correct mistakes and
extend or update references, provided the integrity and authorship of the original paper
remains intact.

The sources includes some straightforward macros for Knuth-style algorithms, and some nice graph drawings in TikZ and TeXGraph. If you find these snippets useful in an academic or educational context, please use and modify them! (Attribute me in the source code comments if you use significant parts.)
In particular, you are welcome to use the images, provided you attribute their source.

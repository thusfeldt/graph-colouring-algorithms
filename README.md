Graph Colouring Algorithms
==========================

This is the source code for

> Thore Husfeldt, *Graph colouring algorithms*. Chapter 13 of Topics in Chromatic Graph Theory, L. W. Beineke and Robin J. Wilson (eds.), Encyclopedia of Mathematics and its Applications 156, Cambridge University Press, ISBN 978-1-107-03350-4, 2015, pp. 277-303.

If you find a mistake, please use the issue tracker to tell me. Or why not just fix it yourself and send me a pull request!

Notable versions:

* **8bcbba0** (May 2014). This is the third and final draft that got sent to the publishers, from which the book version was produced by Cambridge University Press. Tagged as final_draft. (Earlier drafts are also in the repo, tagged as 1st_draft and 2nd_draft.)

* **d6a43c7** (May 2015). This is the publicly available author-typeset version closest to the printed book. It appears as [arXiv:1505.05825v1](http://arxiv.org/abs/1505.05825).
The output tries to mimick the version that appears in the printed book; but I made no effort to synchronize page or line breaks.
 Tagged as arxiv_1505_05825_v1.